import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import LOGIN from './pages/login'
import ADDUSER from './pages/inscription'
import USERS from './pages/users'
import USER from './pages/Modif'
import MODIFME from './pages/Modifme'
function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/' element={<LOGIN />} />
                <Route path='/adduser' element={<ADDUSER />} />
                <Route path='/users' element={<USERS />} />
                <Route path='/user/:id' element={<USER />} />
                <Route path='/userme' element={<MODIFME />} />
            </Routes>

        </BrowserRouter>
    )
}

export default App