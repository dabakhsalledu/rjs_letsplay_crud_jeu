import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { create } from '../controllers/user';
function Inscription() {
    const navigate = useNavigate();
    //creation d'un usestate pour chaque attribut d'un user 
    const [email, setemail] = useState('')
    const [pswd, setpswd] = useState('')
    const [username, setusername] = useState('')
    const [msg, setmsg] = useState('')
    //fonction qui permet de modifier la valeur du email
    const emailchange = (e) => {
        setemail(e.target.value)
    }
    //fonction qui permet de modifier la valeur du mot de passe 
    const pswdchange = (e) => {
        setpswd(e.target.value)
    }
    //fonction qui permet de modifier la valeur du  username
    const userchange = (e) => {
        setusername(e.target.value)
    }
    //creation d'un nouveau user 
    const Createuser = async (e) => {
        try {
            e.preventDefault() //empeche a la page de rafraichir
            //validation des champ s'il ne sont pas vide avant la soumission
            if (email === "") {
                setmsg("email ivalid")
                return
            }
            if (pswd === "") {
                setmsg("mot de passe  ivalid")
                return
            }
            if (username === "") {
                setmsg("username ivalid")
                return
            }
            //json du nouveau user 
            const newuser = {
                'email': email,
                'password': pswd,
                'username': username
            }
            const response = await create(newuser);// appel a la fonction de creation du user avec comme parametre user
            if (response === true) {
                navigate('/users') // si tout se passe bien on part a la page d'acceuil
            }
        } catch (error) {
            setmsg('')
        }
    }
    return (
        <div className='dow1'>
            <img src="./EA.jpg" alt="" width={100} height={100} />
            <h1>Bienvenu <i class="fas fa-thumbs-up blue-icon"></i></h1>
            <h3>Créez-vous un compte, c'est gratuit !!</h3>
            <h6>{msg}</h6>
            <form onSubmit={Createuser} >
                <input type='email' className='input-form' value={email} onChange={emailchange} placeholder='entrez votre email' /><br></br>

                <input type='password' onChange={pswdchange} value={pswd} className='input-form' placeholder='entrez votre mot de passe' /><br></br>

                <input type='text' className='input-form' value={username} onChange={userchange} placeholder='entrez votre username' /><br></br><br></br>

                <button className='btn btn-primary' type='submit' >S'inscrire</button>&nbsp;&nbsp;<Link className='lien' to='/'>Se connecter ?</Link>
            </form>





        </div>
    )
}

export default Inscription