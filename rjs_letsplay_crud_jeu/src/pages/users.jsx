import { React } from 'react'
import { find, win, lost, restart } from '../controllers/user'
import { useState } from 'react'
import { Link } from 'react-router-dom'
function Users() {
    //creation d'un usestate pour chaque attribut d'un user 
    const [users, setusers] = useState([])
    const [isloading, setisloading] = useState(false)
    const isAdmin = sessionStorage.getItem('isAdmin');
    const [firstload, setfirstload] = useState(true)
    //appel de la fonction gagné et perdre aleatoirement
    const gainlostor = async () => {
        setisloading(true)
        let a = Math.floor(Math.random() * 2) // retourne un nombre aleatoire soit 0 ou 1
        let response
        //si a est 1 on appel notre methode win qui ajoute +1 au score du user connecté
        if (a === 1) {
            response = await win()
        }
        //sinon si a est 0 on appel notre methode lost qui decrémente -1 au score du user connecté
        else {
            response = await lost()
        }
        //appel de la fonction qui affiche tout les user avec leurs info a chaque fois
        getData()
        setisloading(false)
    }
    const getData = async () => {
        setisloading(true)
        const users = await find() //retourne tout les users
        setusers(users) //insertion des user dans notre tableau 
        setisloading(false)
    }
    if (firstload) {
        setTimeout(() => {
            getData()
            setfirstload(false)
        }, 3000);

    }
    return (
        <div className=''>
            <img src="./game.jpg" alt="" width={980} height={300} />
            <h2 className='jeu'>LET'S PLAY</h2>

            <h3>Démarer une Partie</h3>
            <div className=''>
                {
                    isAdmin === 'false' ? <button className='btnjeu' onClick={gainlostor}>Jouer!</button> : <h4 className='aver'>désolé vous etes administrateur,
                        vous n'avez pas access au jeux!!
                    </h4>
                }

            </div>
            {
                isAdmin === 'false' && <Link className='link1'
                    to={`/userme`}> <img src="./mod.png" alt="" width={80} height={50} /></Link>
            }
            {users.length === 0 && <button >
                <span class="spinner-border spinner-border-lg"></span>
                veuillez patienter....
            </button>}
            <div className='user'>
                <table className='table' >
                    <tr>
                        <th >Utilisateurs</th>
                        <th className='info'>Informations</th>
                        <th>Score</th>
                        {
                            isAdmin === 'true' && <th className='act'>Action</th>
                        }
                    </tr>
                    {
                        users.map(user => (
                            <tr key={user.id}>
                                <td ><img src="./log.jpg" alt="" width={80} height={50} /></td>
                                <td >{user.username}<br></br> {user.email}</td>
                                <td >{user.score}</td>
                                {
                                    isAdmin === 'true' && <td ><Link className='link'
                                        to={`/user/${user.id}`}><span className='edit-button'><i class="fas fa-pen">
                                        </i></span></Link></td>
                                }
                            </tr>
                        ))
                    }
                </table>
            </div>
            {users.length > 0 && <Link className='link2'
                to={`/`}> <img src="./exit.png" alt="" width={80} height={30} /></Link>}
        </div>
    )
}

export default Users