import React, { useState } from 'react'
import { putMe, getMe, deleteMe, restart } from '../controllers/user';
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { Link } from 'react-router-dom'
function Modifme() {
    const navigate = useNavigate();
    //creation d'un usestate pour chaque attribut d'un user 
    const [users, setusers] = useState([])
    const [email, setemail] = useState('')
    const [isloading, setisloading] = useState(false)
    const [firstload, setfirstload] = useState(true)
    const [username, setusername] = useState('')
    const [msg, setmsg] = useState('')
    const emailchange = (e) => {
        setemail(e.target.value)
    }
    const usernamechange = (e) => {
        setusername(e.target.value)
    }

    const infouser = async (e) => {
        setisloading(true)
        const users = await getMe()
        setusers(users)
        setisloading(false)
    }
    if (firstload) {
        setTimeout(() => {
            infouser()
            setfirstload(false)
        }, 2000);
    }
    //fonction de restart des point de l'utilisateur
    const rest = async () => {
        setisloading(true)
        const response = await restart()
        //si tout se passe bien on retourne a la page d,acceuil
        if (response === true) {
            navigate('/users')
        }
        setisloading(false)
    }
    //fonction qui permet a un user de se supprimer 
    const del = async () => {
        try {
            const response = await deleteMe()
            //si tout se passe bien on retoune a la pge de login
            if (response === true) {
                navigate('/')
            }
        } catch (error) {

        }
    }
    const onsubmit = async (e) => {
        try {

            e.preventDefault()
            if (email === "") {
                setmsg("email ivalid")
                return
            }
            if (username === "") {
                setmsg("mot de passe  ivalid")
                return
            }
            const response = await putMe(username, email)//appel de la methode d'un utilisateur de modifier ses infos
            if (response === true) {
                setTimeout(() => {
                    setmsg('Modification en Cour....')
                    setmsg('')
                    navigate('/users')
                }, 3000);
            }
        } catch (error) {
            setmsg('adresse courriel ou  mot de passe Invalid !! ')
        }
    }
    return (
        <div>
            {
                users.map(user => (
                    <>
                        <h1>Bonjour {user.username}</h1>
                        <h6><button className='btnjeu' onClick={rest}>Restart score</button></h6>
                        <h2>{msg}</h2>
                        <table className='table table-bordered'>
                            <th>Informations</th>
                            <th>score</th>
                            <tr key={user.id}>
                                <td >{user.username}<br></br>{user.email}</td>
                                <td>{user.score}</td>
                            </tr>
                        </table>
                    </>
                ))
            }
            <input type='text' className='input-form' onChange={usernamechange} placeholder='entrez votre nouveau username' /><br></br>

            <input type='email' className='input-form' onChange={emailchange} placeholder='entrez votre nouveau email' /><br></br>
            <button className='btn btn-primary' onClick={onsubmit}>Modifier</button><br></br>
            <hr></hr>
            <hr></hr>
            <h4>Supprimer cet utilisateur ? </h4><h6>Attention Vous  ne ferez plus partie du jeu !!</h6><button className='btn btn-primary' onClick={del}>Supprimer</button><br></br>
            <Link className='link1'
                to={`/users`}> <img src="./ret.png" alt="" width={80} height={50} /></Link>
        </div>
    )
}

export default Modifme